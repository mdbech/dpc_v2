/*
	SimpleGL SDK example modified to prepare for a simple 
	Spring-mass-damper model implementation.
*/

#ifndef _SIMPLEGL_KERNEL_H_
#define _SIMPLEGL_KERNEL_H_

// Cuda utilities
#include "uint_util.hcu"
#include "float_util.hcu"
#include "float_util_device.hcu"

//some constants
#define SPRING_STIFFNESS 1

/**
 * some declerations. this should be ok, as nvcc does automatic inlining
 */
__device__ float getDistance(float3 me, float3 him);
__device__ float3 constantGravity();
__device__ float3 springF(float4* _cur_pos, int idx, uint3 dims);
__device__ float3 doSub(float3 a, float3 b);
__device__ void populateNeighbours(int* num, int* neighbours, int idx, uint3 dims);

__global__ void msd_initialize_kernel( float4 *dataPtr, float3 offset, uint3 dims)
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;

	if( idx<prod(dims) ){

		// 3D coordinate of current particle. 
		const uint3 co = idx_to_co( idx, dims );

		// Output
		float3 pos = uintd_to_floatd(co);
		pos /= uintd_to_floatd(dims);
		pos = pos*2.0f - make_float3(1.0f, 1.0f, 1.0f);
		pos += offset;

		dataPtr[idx] = make_float4( pos.x, pos.y, pos.z, 1.0f );
	}
}

///////////////////////////////////////////////////////////////////////////////
//! Simple kernel to implement numerical integration. EXTEND WITH MSD SYSTEM.
//! @param pos  vertex positiond in global memory
///////////////////////////////////////////////////////////////////////////////
__global__ void msd_kernel( float4 *_old_pos, float4 *_cur_pos, float4 *_new_pos, uint3 dims )
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if( idx<prod(dims) ){

		// Time step size
		const float dt = 0.1f;

		// 3D coordinate of current particle. 
		// Can be offset to access neighbors. E.g.: 
		// upIdx = co_to_idx(co-make_uint3(0,1,0), dims). <-- Be sure to take 
		// speciel care for border cases!
		const uint3 co = idx_to_co( idx, dims );

		// Get the two previous positions
		const float3 old_pos = crop_last_dim(_old_pos[idx]);
		const float3 cur_pos = crop_last_dim(_cur_pos[idx]);

		// Accelerate, function here returns acceleration vector
		//const float3 a = constantGravity();
		const float3 a = springF(_cur_pos, idx, dims);
		
		// Integrate acceleration (forward Euler) to find velocity
		const float3 cur_v = (cur_pos-old_pos)/dt;
		const float3 new_v = cur_v + dt*a; // v'=a

		// Integrate velocity (forward Euler) to find new particle position
		float3 new_pos = cur_pos + dt*new_v; // pos'=v

		// Implement a "floor"
		if( new_pos.y<0 )
			new_pos.y = 0.0f;

		// Output
		_new_pos[idx] = make_float4( new_pos.x, new_pos.y, new_pos.z, 1.0f );
	}
}

__device__ float3 constantGravity(){
	return make_float3( 0.0f, -0.001, 0.0f );//fly away with me
}

/**
 * calculates the f for a single particle
 */
__device__ float3 springF(float4* _cur_pos, int idx, uint3 dims){
	int them[30];
	int num=0;
	populateNeighbours(&num, them, idx, dims);
	float3 total=make_float3( 0.0f, 0.0f, 0.0f );;
	for(int i=0 ; i<num ; i++){
		float3 me = crop_last_dim(_cur_pos[idx]);
		float3 him = crop_last_dim(_cur_pos[them[i]]);
		float d = getDistance(me, him);
		total += SPRING_STIFFNESS * (0 - d) * ((me-him) / d );
	}
	return total;
	
}

/**
 * aux - returns distance between to float3
 */
__device__ float getDistance(float3 me, float3 him){
	return sqrt(dot(me-him,me-him));
}

/**
 * neighbors
 */
__device__ void populateNeighbours(int* num, int* neighbours, int idx, uint3 dims){
	int level=1;//relying on unlooping
	*num=0;//make sure num is initilised
	for(int x=-level ; x<=level ; x++){
		for(int y=-level ; y<=level ; y++){
			for(int z=-level ; z<=level ; z++){
				if(0==x==y==z)
					continue;
				//do boundry check here
				neighbours[*num] = co_to_idx(make_uint3(x,y,z), dims);
				cuPrintf("idx:%d,pos.x:",neighbours[*num]); 
				*num++;
			}
		}
	}
}

#endif // #ifndef _SIMPLEGL_KERNEL_H_
