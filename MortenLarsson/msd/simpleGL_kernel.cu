/*
	SimpleGL SDK example modified to prepare for a simple 
	Spring-mass-damper model implementation.
*/

#ifndef _SIMPLEGL_KERNEL_H_
#define _SIMPLEGL_KERNEL_H_

// Cuda utilities
#include "uint_util.hcu"
#include "float_util.hcu"
#include "float_util_device.hcu"
#include "rotate.cuh"
#include "rotate.cu"


__global__ void msd_initialize_kernel( float4 *dataPtr, float3 offset, uint3 dims, float angle)
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;

	if( idx<prod(dims) ){

		// 3D coordinate of current particle. 
		const uint3 co = idx_to_co( idx, dims );
		// Output
		float3 pos = uintd_to_floatd(co);

		//Rotate
		tilt_y(&pos, angle);

		pos /= uintd_to_floatd(dims);
		pos = pos*2.0f - make_float3(1.0f, 1.0f, 1.0f);
		pos += offset;
	//	pos -= make_float3(0.0f,1.3f,0.0f);


		dataPtr[idx] = make_float4( pos.x, pos.y, pos.z, 1.0f );
	}
}

///////////////////////////////////////////////////////////////////////////////
//! Simple kernel to implement numerical integration. EXTEND WITH MSD SYSTEM.
//! @param pos  vertex positiond in global memory
///////////////////////////////////////////////////////////////////////////////


__device__ float distance(float3 pt1, float3 pt2)
{
  float3 v = pt2 - pt1;
  return sqrt(dot(v,v));
}

__device__ float distance(uint3 co1, uint3 co2)
{
  float3 v = make_float3((float) co1.x, (float)co1.y, (float)co1.z) - make_float3((float) co2.x, (float)co2.y, (float)co2.z);
  return sqrt(dot(v,v));
}

__device__ void get_neighbours(unsigned int *neighbourIdx,unsigned int *num_of_neighbours, uint3 co, uint3 dims, float sqrt_v) {
	for(int x = -1; x <= 1; x++) {
		for(int y = -1; y <= 1; y++) {
			for(int z = -1; z <= 1; z++) {
				int cox = (int16_t)co.x+x;
				int coy = (int16_t)co.y+y;
				int coz = (int16_t)co.z+z;
				if(cox < dims.x && coy < dims.y && coz < dims.z && cox >= 0 && coy >= 0 && coz >= 0 && !(0==x && 0==y && 0==z)) {
					if(distance(co, make_uint3(cox,coy,coz)) <= sqrt(sqrt_v)) {
						neighbourIdx[*num_of_neighbours] = co_to_idx(make_uint3(cox,coy,coz), dims);
						*num_of_neighbours = *num_of_neighbours +1;	
					}
				}
			}
		}
	}
}

__global__ void msd_kernel( float4 *_old_pos, float4 *_cur_pos, float4 *_new_pos, uint3 dims, float sqrt_v, float damping)
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if( idx<prod(dims) ){

		// Time step size
		const float dt = 0.1f;

		// 3D coordinate of current particle. 
		const uint3 co = idx_to_co( idx, dims );


		//Variables for holder neighbour information.
		unsigned int num_of_neighbours = 0;  
		unsigned int neighbour_idx_list[32];
	
		//Make function pointers.
		unsigned int *fp1 = neighbour_idx_list;
		unsigned int *fp2 = &num_of_neighbours;	

		//Go get neighbour information (sqrt(2) neighbours).
		get_neighbours(fp1,fp2, co, dims, sqrt_v);

		// Get the two previous positions
		const float3 old_pos = crop_last_dim(_old_pos[idx]);
		const float3 cur_pos = crop_last_dim(_cur_pos[idx]);

		//Calculate acceleration
		float3 a = make_float3(0.0f,0.0f,0.0f);
		float d;
		float3 xi_xj;
		for(int i=0; i < (num_of_neighbours); i++) {
			d = distance(crop_last_dim(_cur_pos[neighbour_idx_list[i]]), cur_pos);
			xi_xj = cur_pos-crop_last_dim(_cur_pos[neighbour_idx_list[i]]);
			a += k*(l-d)*(xi_xj / d);
		}
	
		//Verlet integration
		float3 new_pos = (2-damping)*cur_pos-(1-damping)*old_pos+a*dt*dt;

		// Output
		_new_pos[idx] = make_float4( new_pos.x, new_pos.y, new_pos.z, 1.0f );
	}
}


#endif // #ifndef _SIMPLEGL_KERNEL_H_
