/*
	SimpleGL SDK example modified to prepare for a simple 
	Spring-mass-damper model implementation.
*/

#ifndef _SIMPLEGL_KERNEL_H_
#define _SIMPLEGL_KERNEL_H_

// Cuda utilities
#include "uint_util.hcu"
#include "float_util.hcu"
#include "float_util_device.hcu"

__global__ void msd_initialize_kernel( float4 *dataPtr, float3 offset, uint3 dims )
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;

	if( idx<prod(dims) ){

		// 3D coordinate of current particle. 
		const uint3 co = idx_to_co( idx, dims );

		// Output
		float3 pos = uintd_to_floatd(co);
		pos /= uintd_to_floatd(dims);
		pos = pos*2.0f - make_float3(1.0f, 1.0f, 1.0f);
		pos += offset;

		dataPtr[idx] = make_float4( pos.x, pos.y, pos.z, 1.0f );
	}
}

///////////////////////////////////////////////////////////////////////////////
//! Offset index
///////////////////////////////////////////////////////////////////////////////
__device__ float3 offset(float4 *_cur_pos, const uint3 co, const int x, const int y, const int z, const uint3 dims)
{
	return crop_last_dim(_cur_pos[co_to_idx(make_uint3(co.x+x,co.y+y,co.z+z), dims)]);
}
///////////////////////////////////////////////////////////////////////////////
//! Equation 1.2
///////////////////////////////////////////////////////////////////////////////
__device__ float3 spring_force(const float3 x_i, const float3 x_j, const float l_ij)
{
	float3 xi_xj = x_i - x_j;
	float d = sqrt(dot(xi_xj,xi_xj));
	return k * (l_ij - d) * (xi_xj / d);
}

///////////////////////////////////////////////////////////////////////////////
//! Simple kernel to implement numerical integration. EXTEND WITH MSD SYSTEM.
//! @param pos  vertex positiond in global memory
///////////////////////////////////////////////////////////////////////////////
__global__ void msd_kernel( float4 *_old_pos, float4 *_cur_pos, float4 *_new_pos, uint3 dims )
{
	// Index in position array
	const unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if( idx<prod(dims) ){

		// Time step size
		const float dt = 0.1f;

		// 3D coordinate of current particle. 
		// Can be offset to access neighbors. E.g.: upIdx = co_to_idx(co-make_uint3(0,1,0), dims). <-- Be sure to take speciel care for border cases!
		const uint3 co = idx_to_co( idx, dims );

		// Get the two previous positions
		const float3 old_pos = crop_last_dim(_old_pos[idx]);
		const float3 cur_pos = crop_last_dim(_cur_pos[idx]);

		// Accelerate (constant gravity)
		// const float _a = -0.001f;

		// Acceleration variable definition
		float3 a = make_float3( 0.0f, 0.0f, 0.0f );

		// sqrt(1) - Neighbouring particles (6 particles)
		if( co.x > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,-1,0,0,dims), l_sqrt1);
		if( co.x < dims.x-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,1,0,0,dims), l_sqrt1);
		if( co.y > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,0,-1,0,dims), l_sqrt1);
		if( co.y < dims.y-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,0,1,0,dims), l_sqrt1);
		if( co.z > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,0,0,-1,dims), l_sqrt1);
		if( co.z < dims.z-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,0,0,1,dims), l_sqrt1);

		// sqrt(2) - Neighbouring particles (12 particles)
		if( co.x > 0 && co.y > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,-1,-1,0,dims), l_sqrt2);
		if( co.x > 0 && co.y < dims.y-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,-1,1,0,dims), l_sqrt2);
		if( co.x > 0 && co.z > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,-1,0,-1,dims), l_sqrt2);
		if( co.x > 0 && co.z < dims.z-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,-1,0,1,dims), l_sqrt2);
		if( co.x < dims.x-1 && co.y > 0 )	a += spring_force(cur_pos, offset(_cur_pos,co,1,-1,0,dims), l_sqrt2);
		if( co.x < dims.x-1 && co.y < dims.y-1) a += spring_force(cur_pos, offset(_cur_pos,co,1,1,0,dims), l_sqrt2);
		if( co.x < dims.x-1 && co.z > 0 )	a += spring_force(cur_pos, offset(_cur_pos,co,1,0,-1,dims), l_sqrt2);
		if( co.x < dims.x-1 && co.z < dims.z-1)	a += spring_force(cur_pos, offset(_cur_pos,co,1,0,1,dims), l_sqrt2);
		if( co.y > 0 && co.z > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,0,-1,-1,dims), l_sqrt2);
		if( co.y > 0 && co.z < dims.z-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,0,-1,1,dims), l_sqrt2);
		if( co.y < dims.y-1 && co.z > 0 )	a += spring_force(cur_pos, offset(_cur_pos,co,0,1,-1,dims), l_sqrt2);
		if( co.y < dims.y-1 && co.z < dims.z-1)	a += spring_force(cur_pos, offset(_cur_pos,co,0,1,1,dims), l_sqrt2);

		// sqrt(3) - Neighbouring particles (8 particles)
		if( co.x > 0 && co.y > 0 && co.z > 0)			a += spring_force(cur_pos, offset(_cur_pos,co,-1,-1,-1,dims), l_sqrt3);
		if( co.x > 0 && co.y > 0 && co.z < dims.z-1 )		a += spring_force(cur_pos, offset(_cur_pos,co,-1,-1,1,dims), l_sqrt3);
		if( co.x > 0 && co.y < dims.y-1 && co.z > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,-1,1,-1,dims), l_sqrt3);
		if( co.x > 0 && co.y < dims.y-1 && co.z < dims.z-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,-1,1,1,dims), l_sqrt3);
		if( co.x < dims.x-1 && co.y > 0 && co.z > 0 )		a += spring_force(cur_pos, offset(_cur_pos,co,1,-1,-1,dims), l_sqrt3);
		if( co.x < dims.x-1 && co.y > 0 && co.z < dims.y-1 )	a += spring_force(cur_pos, offset(_cur_pos,co,1,-1,1,dims), l_sqrt3);
		if( co.x < dims.x-1 && co.y < dims.y-1 && co.z > 0 )	a += spring_force(cur_pos, offset(_cur_pos,co,1,1,-1,dims), l_sqrt3);
		if( co.x < dims.x-1 && co.y < dims.y-1 && co.z < dims.z-1) a += spring_force(cur_pos, offset(_cur_pos,co,1,1,1,dims), l_sqrt3);

		// Integrate acceleration (forward Euler) to find velocity
		const float3 cur_v = (cur_pos-old_pos)/dt;
		const float3 new_v = cur_v + dt*a; // v'=a

		// Integrate velocity (forward Euler) to find new particle position
		//float3 new_pos = cur_pos + dt*new_v; // pos'=v
		float3 new_pos = (2 - lambda)*cur_pos - (1 - lambda)*old_pos + a*dt*dt;

		// Implement a "floor"
		if( new_pos.y<0 )
			new_pos.y = 0.0f;
		
		// Make sure that all threads are at the same point
		//__syncthreads();

		// Output
		_new_pos[idx] = make_float4( new_pos.x, new_pos.y, new_pos.z, 1.0f );

		// Make sure that all threads are at the same point
		//__syncthreads();
	}
}

#endif // #ifndef _SIMPLEGL_KERNEL_H_
